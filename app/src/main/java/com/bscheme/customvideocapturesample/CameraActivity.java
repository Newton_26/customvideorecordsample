package com.bscheme.customvideocapturesample;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CameraActivity extends AppCompatActivity {

    FrameLayout preview;
    Camera mCamera;
    CameraPreview mCameraPreview;
    MediaRecorder mediaRecorder;
    private boolean isRecording = false , isFlashlightON = false;
    ImageButton ibCapture, ibSwitchCamera,ibFlash;

    private Camera.Parameters parameters;
    private List<Camera.Size> supportedPreviewSizes;
    private List<Camera.Size> supportedVideoSizes;
    private int desiredwidth=1280, desiredheight=720; // 1280:720 <> 640:360

    Camera.Size optimalPreviewSize ;
    Camera.Size optimalVideoSize;

    static int currentCameraId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_camera);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        preview = (FrameLayout) findViewById(R.id.preview);
        ibSwitchCamera = (ImageButton) findViewById(R.id.ib_switch_camera);
        ibFlash = (ImageButton) findViewById(R.id.ib_flash);

        if (checkCameraHardware(this)) {
            mCamera = getCameraInstance(currentCameraId);
            try {
                setCameraPreviewParameter();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mCameraPreview = new CameraPreview(this,mCamera);
            preview.addView(mCameraPreview);
        }
        ibSwitchCamera.setOnClickListener(switchCameraListener);
        ibFlash.setOnClickListener(flashLightTurnOnListener);

        // Add a listener to the Capture button
        ibCapture = (ImageButton) findViewById(R.id.ib_capture);
        ibCapture.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isRecording) {
                            Log.i("capture btn", "click 1= " + isRecording);
                            // stop recording and release camera
                            mediaRecorder.stop();  // stop the recording
                            releaseMediaRecorder(); // release the MediaRecorder object
                            mCamera.lock();         // take camera access back from MediaRecorder

                            ibCapture.setImageResource(R.drawable.video_record);

                            //active switch camera
                            ibSwitchCamera.setAlpha(1.0f);
                            ibSwitchCamera.setClickable(true);

                            // inform the user that recording has stopped
                            isRecording = false;
                        } else {
                            Log.i("capture btn", "click 2 = " + isRecording);
                            // initialize video camera
                            if (prepareRecorder()) {
                                Log.i("recorder", "prepared");
                                // Camera is available and unlocked, MediaRecorder is prepared,
                                // now you can start recording
                                mediaRecorder.start();

                                ibCapture.setImageResource(R.drawable.abc_btn_radio_to_on_mtrl_015);
                                //inactive switch camera
                                ibSwitchCamera.setAlpha(0.4f);
                                ibSwitchCamera.setClickable(false);

                                // inform the user that recording has started
                                isRecording = true;
                            } else {
                                // prepare didn't work, release the camera
                                releaseMediaRecorder();
                                // inform user
                            }
                        }
                    }
                }
        );
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (mCamera == null) {
            mCamera = getCameraInstance(currentCameraId);
            try {
                setCameraPreviewParameter();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mCameraPreview.setCamera(mCamera);
        }

    }
    @Override
    protected void onPause() {
        super.onPause();
        releaseMediaRecorder();       // if you are using MediaRecorder, release it first
        releaseCamera();
        //active switch camera
        ibSwitchCamera.setAlpha(1.0f);
        ibSwitchCamera.setClickable(true);

        ibCapture.setImageResource(R.drawable.video_record);
    }

    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri(){
        return Uri.fromFile(getOutputMediaFile());
    }

    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "Dropia captures");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;


        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "dropia_"+ timeStamp + ".mp4");


        return mediaFile;
    }

    private void releaseMediaRecorder(){
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            mCamera.lock();           // lock camera for later use
        }
    }

    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }

    /** Check if this device has a camera */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }
    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(int currentCameraId){
        Camera c = null;
        try {
            c = Camera.open(currentCameraId); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    boolean prepareRecorder(){

        if (mCamera == null)
            mCamera = getCameraInstance(currentCameraId);

        try {
            setCameraPreviewParameter();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mediaRecorder = new MediaRecorder();

        mCamera.unlock();
        mediaRecorder.setCamera(mCamera);

        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));

        mediaRecorder.setVideoSize(optimalPreviewSize.width, optimalPreviewSize.height);
        mediaRecorder.setOutputFile(getOutputMediaFileUri().getPath().toString());

        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            releaseMediaRecorder();
            return false;
        }
        return true;
    }


    private void setCameraPreviewParameter() throws Exception{
        parameters = mCamera.getParameters();
        supportedPreviewSizes = parameters.getSupportedPreviewSizes();
        supportedVideoSizes = parameters.getSupportedVideoSizes();

        optimalPreviewSize = getOptimalPreviewSize(supportedPreviewSizes, desiredwidth, desiredheight);
        optimalVideoSize = getOptimalPreviewSize(supportedVideoSizes, desiredwidth, desiredheight);

        parameters.setPreviewSize(optimalPreviewSize.width, optimalPreviewSize.height);
        mCamera.setParameters(parameters);
    }
    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h){

        final double ASPECT_TOLERANCE = 0.2;
        double targetRatio = (double) w / h;
        if (sizes == null)
            return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        // Try to find an size match aspect ratio and size
        for (Camera.Size size : sizes) {
            Log.d("Camera", "Checking size " + size.width + "w " + size.height
                    + "h");
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        // Cannot find the one match the aspect ratio, ignore the
        // requirement
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    View.OnClickListener switchCameraListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // get the number of cameras
            if (!isRecording) {
                int camerasNumber = Camera.getNumberOfCameras();
                if (camerasNumber > 1) {
                    // release the old camera instance
                    // switch camera, from the front and the back and vice versa

                    releaseCamera();
                    ibSwitchCamera.setAlpha(1.0f);
                    ibSwitchCamera.setClickable(true);

                    if (currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK){
                        currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
                        ibFlash.setAlpha(0.4f);
                        ibFlash.setClickable(false);
                    }else {
                        currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
                        ibFlash.setAlpha(1.0f);
                        ibFlash.setClickable(true);
                    }
                    mCamera = getCameraInstance(currentCameraId);
                    try {
                        setCameraPreviewParameter();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mCameraPreview.refreshCamera(mCamera);
                } else {
                    ibSwitchCamera.setAlpha(0.4f);
                    ibSwitchCamera.setClickable(false);
                    Toast toast = Toast.makeText(CameraActivity.this, "Sorry, your phone has only one camera!", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        }
    };

    View.OnClickListener flashLightTurnOnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // flashlight feature check
            if (mCamera != null && getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
                if (currentCameraId != Camera.CameraInfo.CAMERA_FACING_FRONT && isFlashlightON) {
                    parameters = mCamera.getParameters();
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    mCamera.setParameters(parameters);
//                    mCameraPreview.refreshCamera(mCamera);
                    isFlashlightON = false;
                } else {
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    mCamera.setParameters(parameters);
//                    mCameraPreview.refreshCamera(mCamera);
                    isFlashlightON = true;
                }
            }else {
                Toast toast = Toast.makeText(CameraActivity.this, "No flashlight !", Toast.LENGTH_LONG);
                toast.show();
            }
        }
    };

}
